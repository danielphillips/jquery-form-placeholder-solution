jQuery is required for this script. Include the javascript at the end of the page.

Example usage:

```

<form method="post">
	<h1>Login</h1>
    <input type="hidden" name="login" value="1" />
    
	<input type="text" name="username" placeholder="Username" value="" />
	
    <input type="password" name="password" placeholder="Password" value="" />
    
    <label><input type="checkbox" name="remember" /> Remember me</label>
    
    <input type="submit" name="login" value="Login" />
</form>


```